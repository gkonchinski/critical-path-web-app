$(document).ready(function(){

    $("#loginButton").on("click", function(){
        let username = $("#username").val();
        let password = $("#password").val();

        checkLoginCredentials(username, password).then(function(data){
            // console.log(data);
            // Free of errors, by submitting all inputs
            if(data["errors"]["username"] == "" && data["errors"]["password"] == ""){

                //Hide all errors
                $("#password_error").hide();
                $("#username_error").hide();

                // If they are a valid user
                if(data["user"]["is_valid_user"] == 1){
                    localStorage.setItem("jwt_token", data["user"]["jwt_token"]);

                    //Don't trust this value for security, just for convenience. All necessary API endpoints will check the token anyway
                    localStorage.setItem("user_role", data["user"]["user_role"]);
                    localStorage.setItem("uin", data["user"]["uin"]);

                    if(data["user"]["user_role"] == 2){
                        window.location.href = "../student/student_home.html";
                    }
                    else if(data["user"]["user_role"] == 3){
                        window.location.href = "../advisor/advisor.html";
                    }
                    else if(data["user"]["user_role"] == 4){
                        window.location.href = "../admin/admin.html";
                    }
                }
                else{
                    Swal.fire({
                        title: 'Error!',
                        text: data["errors"]["invalid_user"],
                        icon: 'error',
                        confirmButtonText: 'Cool'
                      })
                }
            }
            else{
                if(data["errors"]["password"] != ""){
                    $("#password_error").html(data["errors"]["password"]);
                    $("#password_error").show();
                }
                else{
                    $("#password_error").hide();
                }
                if(data["errors"]["username"] != ""){
                    $("#username_error").html(data["errors"]["username"]);
                    $("#username_error").show();
                }
                else{
                    $("#username_error").hide();
                }
            }
        }).catch(function(error){
            // TODO add error handling
        })
    })
})

function checkLoginCredentials(username, password){
    return new Promise((resolve, reject) => {
        $.ajax({
            url:"https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/users/login",
            // url: "http://localhost:9000/users/login",
            type: "POST",
            data: {
                username: username,
                password: password
            },
            success: function(response){
               resolve(response);
            },
            error: function(error){
                reject(error);
            }
        })
    })
}

function guestAlert() {
    if (confirm("Warning! Any plans made as a guest will not be saved!")){
        window.location.href="../guest/guest_home.html";
    }else{
        window.location.href="./log_in.html";
    }
  }
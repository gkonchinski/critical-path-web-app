if ( document.URL.includes("student.html") ) {
    if(localStorage.getItem("user_role") != 2){
        window.location.href = "../generic/log_in.html"
    }
}
if(document.URL.includes("advisor")){
    if(localStorage.getItem("user_role") != 3){
        window.location.href = "../generic/log_in.html"
    }
}
if(document.URL.includes("admin")){
    if(localStorage.getItem("user_role") != 4){
        window.location.href = "../generic/log_in.html"
    }
}
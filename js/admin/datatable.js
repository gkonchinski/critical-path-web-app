$(function() {
    
    if(document.URL.includes("maintenance.html")){
        $("#accountMaintenanceGrid").jsGrid({
            height: "auto",
            width: "100%",
     
            // filtering: true,
            editing: true,
            sorting: true,
            paging: true,
            autoload: true,
     
            pageSize: 15,
            pageButtonCount: 5,
            
            // deleteConfirm: "Do you really want to delete the user?",
     
            controller: {
                loadData: function(){
                    return new Promise((resolve, reject) => {
                        $.ajax({
                            // url: "http://localhost:9000/users/get-users",
                            url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/users/get-users",
                            type: "GET",
                            dataType: 'json',
                            headers: {"Authorization": localStorage.getItem('jwt_token')},
                            success: function(data){
                                resolve(data["userData"]);
                            },
                            error: function(error){
                                reject(error);
                            }
                        });
                    });
                },
                // insertItem: $.noop,
                updateItem: function(item){
                    return $.ajax({
                        // url: "http://localhost:9000/users/edit-user",
                        url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/users/edit-user",
                        type: "POST",
                        dataType: 'json',
                        headers: {"Authorization": localStorage.getItem('jwt_token')},
                        data: item
                    })
                }
                // deleteItem: $.noop
            },
            fields: [
                { name: "role_id", title:"role", type:"select", width: 50, items: [{Name: "Student", Id: 2}, {Name: "Advisor", Id:3}, {Name: "Administrator", Id:4}], valueField: "Id", textField: "Name"},
                { name: "email", title:"Email", type: "text", width: 150, editing: false },
                { name: "name", title:"Name", type: "text", width: 50, editing: false },
                { name: "uin", title: "UIN", type: "number", width: 50, editing: false, align: "left" },
                { name: "phone", title: "Phone Number", type: "text", width: 50, editing: false},
                { 
                    type: "control", 
                    deleteButton: false
                }
            ]
        });
    }
    else if(document.URL.includes("enrollment.html")){
         loadingDiv = $('#loadingDiv');
        $(loadingDiv).hide();  
        $(document).ajaxStart(function() {
            $(loadingDiv).show();
        })
        .ajaxStop(function() {
            $(loadingDiv).hide();
        });
        var catalogs;
        var subjects;
        $.ajax({
            url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/get-lookups",
            type: "GET",
            dataType: 'json',
            headers: {"Authorization": localStorage.getItem('jwt_token')},
        }).done(function(lookups){
            subjects=lookups.subjects;
            subjects.unshift({ subject: "" });
            catalogs=lookups.catalogs;
            catalogs.unshift({ catalog_year: ""});
            terms=lookups.terms;
            terms.unshift({ name: ""});
            course_catalog_select = $("#course_catalog_select");
            $.each(lookups.catalogs,function(index,catalog){
                if(catalog.catalog_year == ""){
                    $(course_catalog_select).append('<option value="" selected>Select a catalog year to update</option>');
                } else {
                    $(course_catalog_select).append('<option value="'+catalog.catalog_year+'">'+catalog.catalog_year+'</option>');
                }
            });
            course_subject_select = $("#course_subject_select");
            schedule_subject_select = $("#schedule_subject_select");
            $.each(lookups.subjects,function(index,subject){
                if(subject.subject == ""){
                    $(course_subject_select).append('<option value="" selected>Select a subject to update</option>');
                    $(schedule_subject_select).append('<option value="" selected>Select a subject to update</option>');
                } else {
                    $(course_subject_select).append('<option value="'+subject.subject+'">'+subject.subject+'</option>');
                    $(schedule_subject_select).append('<option value="'+subject.subject+'">'+subject.subject+'</option>');
                }
            });
            schedule_term_select = $("#schedule_term_select");
            $.each(lookups.terms,function(index,term){
                if(term.name == ""){
                    $(schedule_term_select).append('<option value="" selected>Select a term to update</option>');
                } else {
                    $(schedule_term_select).append('<option value="'+term.name+'">'+term.name+'</option>');
                }
            });
            $("#scraperMaintenanceGrid").jsGrid({
                height: "auto",
                width: "100%",
        
                filtering: true,
                editing: false,
                sorting: true,
                paging: true,
                autoload: true,
                loadIndicationDelay: 100,
                pageLoading: true,
                pageSize: 15,
                pageButtonCount: 5,
                pagerContainer: "#scraperMaintenanceGridPager",
                loadIndicator: function(config) {
                    return {
                        show: function() {
                        },
                        hide: function() {
                        }
                    };
                },


                controller: {
                    loadData: function(filter){
                        return new Promise((resolve, reject) => {
                            $.ajax({
                                url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/get-courses",
                                type: "GET",
                                dataType: 'json',
                                headers: {"Authorization": localStorage.getItem('jwt_token')},
                                data: filter,
                                success: function(data){
                                    resolve(data);
                                },
                                error: function(error){
                                    reject(error);
                                }
                            });
                        });    
                    },
                },
                fields: [
                    { name: "subject", editing: false, filtering: true, title:"Course Subject", width: "5%", align: "left", type: "select", items: subjects, valueField: "subject", textField: "subject"},
                    { name: "course", editing: false, filtering: true, title:"Course Number", type: "text", width: "5%", align: "left"},
                    { name: "catalog_year", editing: false, filtering: true, sorting: false, title: "Catalog Year", width: "5%", align: "left", type: "select", items: catalogs, valueField: "catalog_year", textField: "catalog_year"},
                    { name: "title", editing: false, filtering: true, sorting: true, title: "Title", type: "text", width: "15%", align: "left"},
                    { name: "description", editing: false, filtering: true, sorting: true, title: "Description", type: "text", width: "25%", align: "left"},
                    { name: "prereqs", editing: false, filtering: true, sorting: true, title: "Prerequisites", type: "text", width: "15%", align: "left"},
                    { name: "credits", editing: false, filtering: false, sorting: true, title: "Credits", type: "text", width: "5%", align: "left"},
                    { 
                        type: "control",
                        itemTemplate: function(value, item) {
                            var $result = $([]);
        
                            if(item.Editable) {
                                $result = $result.add(this._createEditButton(item));
                            }
                
                            if(item.Deletable) {
                                $result = $result.add(this._createDeleteButton(item));
                            }
                
                            return $result;
                        }
                    }
                ]
            });
            $("#enrollmentMaintenanceGrid").jsGrid({
                height: "auto",
                width: "100%",
        
                filtering: true,
                editing: false,
                sorting: true,
                paging: true,
                autoload: true,
                loadIndicationDelay: 100,
                pageLoading: true,
                pageSize: 15,
                pageButtonCount: 5,
                pagerContainer: "#enrollmentMaintenanceGridPager",
                loadIndicator: function(config) {
                    return {
                        show: function() {
                        },
                        hide: function() {
                        }
                    };
                },


                controller: {
                    loadData: function(filter){
                        return new Promise((resolve, reject) => {
                            $.ajax({
                                url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/get-enrollments",
                                type: "GET",
                                dataType: 'json',
                                headers: {"Authorization": localStorage.getItem('jwt_token')},
                                data: filter,
                                success: function(data){
                                    resolve(data);
                                },
                                error: function(error){
                                    reject(error);
                                }
                            });
                        });    
                    },
                },
                fields: [
                    { name: "term", editing: false, filtering: true, sorting: false, title: "Term", width: "5%", align: "left", type: "select", items: terms, valueField: "name", textField: "name"},
                    { name: "uin", editing: false, filtering: true, sorting: true, title: "UIN", type: "text", width: "15%", align: "left"},
                    { name: "email", editing: false, filtering: true, sorting: true, title: "Email", type: "text", width: "25%", align: "left"},
                    { name: "fullname", editing: false, filtering: true, sorting: true, title: "Full Name", type: "text", width: "15%", align: "left"},
                    { name: "subject", editing: false, filtering: true, title:"Course Subject", width: "5%", align: "left", type: "select", items: subjects, valueField: "subject", textField: "subject"},
                    { name: "course", editing: false, filtering: true, title:"Course Number", type: "text", width: "5%", align: "left"},
                    { 
                        type: "control",
                        itemTemplate: function(value, item) {
                            var $result = $([]);
        
                            if(item.Editable) {
                                $result = $result.add(this._createEditButton(item));
                            }
                
                            if(item.Deletable) {
                                $result = $result.add(this._createDeleteButton(item));
                            }
                            return $result;
                        }
                    }
                ]
            });
            $("#scheduleMaintenanceGrid").jsGrid({
                height: "auto",
                width: "100%",
        
                filtering: true,
                editing: false,
                sorting: true,
                paging: true,
                autoload: true,
                loadIndicationDelay: 100,
                pageLoading: true,
                pageSize: 15,
                pageButtonCount: 5,
                pagerContainer: "#scheduleMaintenanceGridPager",
                loadIndicator: function(config) {
                    return {
                        show: function() {
                        },
                        hide: function() {
                        }
                    };
                },
                controller: {
                    loadData: function(filter){
                        return new Promise((resolve, reject) => {
                            $.ajax({
                                url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/get-schedules",
                                type: "GET",
                                dataType: 'json',
                                headers: {"Authorization": localStorage.getItem('jwt_token')},
                                data: filter,
                                success: function(data){
                                    resolve(data);
                                },
                                error: function(error){
                                    reject(error);
                                }
                            });
                        });    
                    },
                },
                fields: [
                    { name: "crn", editing: false, filtering: true, sorting: true, title: "CRN", type: "text", width: "15%", align: "left"},
                    { name: "term", editing: false, filtering: true, sorting: false, title: "Term", width: "20%", align: "left", type: "select", items: terms, valueField: "name", textField: "name"},
                    { name: "subject", editing: false, filtering: true, title:"Course Subject", width: "15%", align: "left", type: "select", items: subjects, valueField: "subject", textField: "subject"},
                    { name: "course", editing: false, filtering: true, title:"Course Number", type: "text", width: "15%", align: "left"},
                    { name: "total", editing: false, filtering: false, sorting: true, title: "Total Seats", type: "text", width: "15%", align: "left"},
                    { name: "available", editing: false, filtering: false, sorting: true, title: "Available Seats", type: "text", width: "15%", align: "left"},
                    { 
                        type: "control",
                        itemTemplate: function(value, item) {
                            var $result = $([]);
        
                            if(item.Editable) {
                                $result = $result.add(this._createEditButton(item));
                            }
                
                            if(item.Deletable) {
                                $result = $result.add(this._createDeleteButton(item));
                            }
                
                            return $result;
                        }
                    }
                ]
            });
        });
        $("#jobMaintenanceGrid").jsGrid({
            height: "auto",
            width: "100%",
            filtering: false,
            editing: false,
            sorting: true,
            paging: true,
            autoload: true,
            loadIndicationDelay: 100,
            pageLoading: false,
            pageSize: 15,
            pageButtonCount: 5,
            pagerContainer: "#jobMaintenanceGridPager",
            loadIndicator: function(config) {
                return {
                    show: function() {
                    },
                    hide: function() {
                    }
                };
            },


            controller: {
                loadData: function(filter){
                    return new Promise((resolve, reject) => {
                        $.ajax({
                            url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/jobs/get-job-data",
                            type: "GET",
                            dataType: 'json',
                            headers: {"Authorization": localStorage.getItem('jwt_token')},
                            data: filter,
                            success: function(data){
                                resolve(data.aaData);
                            },
                            error: function(error){
                                reject(error);
                            }
                        });
                    });    
                },
            },
            fields: [
                { name: "job_title", editing: false, filtering: false, sorting: true, title: "Job Title", type: "text", width: "15%", align: "left"},
                { name: "job_overview", editing: false, filtering: false, sorting: true, title: "Job Description", type: "text", width: "45%", align: "left"},
                { name: "entry_level_education", editing: false, filtering: false, sorting: true, title: "Education", type: "text", width: "10%", align: "left"},
                { name: "work_related_experience", editing: false, filtering: false, sorting: true, title: "Experience", type: "text", width: "10%", align: "left"},
                { name: "med_salary", editing: false, filtering: false, sorting: true, title: "Median Salary", type: "text", width: "10%", align: "left",
                    itemTemplate: function(value, item) {
                        return numberWithCommas(value);
                    }
                },
                { name: "number_jobs", editing: false, filtering: false, sorting: true, title: "# of Jobs", type: "text", width: "10%", align: "left",
                    itemTemplate: function(value, item) {
                        return numberWithCommas(value);
                    }
                }
            ]
        });
    }
}); 

function getAllUserData(){
    // return new Promise((resolve, reject) => {
    //     $.ajax({
    //         url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/users/get-users",
    //         type: "GET",
    //         success: function(data){
    //             resolve(data);
    //         },
    //         error: function(error){
    //             reject(error);
    //         }
    //     });
    // });
}
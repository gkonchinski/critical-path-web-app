$("#submitEnrollmentUpload").on("click", function(){
    
    let data = new FormData();
    data.append("file", $(':file')[0].files[0]);
    
    return $.ajax({
        // url: 'http://localhost:9000/data-entry/upload-enrollment-data',
        url: 'https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/data-entry/upload-enrollment-data',
        headers: {"Authorization": localStorage.getItem('jwt_token')},
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
    });
});

$("#runJobScraper").on("click", function(){

    $(this).prop("disabled", true);
    // add spinner to button
    $(this).html(
      `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Scraping BLS Data`
    );

    $.ajax({
        // url: "http://localhost:9000/data-entry/run-bls-scraper",
        url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/data-entry/run-bls-scraper",
        headers: {"Authorization": localStorage.getItem('jwt_token')},
        method: "GET"
    }).then(function(data){
        Swal.fire(
            'BLS Data Successfully Updated!',
            'success'
        );

        $("#runJobScraper").prop("disabled", false);
        $("#runJobScraper").html("Scrape BLS");
    });
});

function uploadEnrollmentFile(){
    let data = new FormData();
    data.append("file", $(':file')[0].files[0]);       
    return $.ajax({
        // url: 'http://localhost:9000/courses/update-enrollments',
        url: 'https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/update-enrollments',
        headers: {"Authorization": localStorage.getItem('jwt_token')},
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
    }).done(function(data){
        $("#enrollmentMaintenanceGrid").jsGrid("loadData");
        Swal.fire(
            'Server Processing Complete',
            'Enrollment Data Successfully Updated!',
            'success'
        );
    });
}

function runJobScraper(){
    $.ajax({
        // url: "http://localhost:9000/data-entry/run-bls-scraper",
        url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/data-entry/run-bls-scraper",
        headers: {"Authorization": localStorage.getItem('jwt_token')},
        method: "GET"
    }).done(function(data){
        $("#jobMaintenanceGrid").jsGrid("loadData");
        Swal.fire(
            'Server Processing Complete',
            'Job Data Successfully Updated!',
            'success'
        );
    });    
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function updateCoursesUsingWebScraper(){
    var selectedCatalogYear = $(course_catalog_select).children("option:selected").val();
    var selectedSubject = $(course_subject_select).children("option:selected").val();
    //console.log(selectedCatalogYear,'selectedCatalogYear');
    //console.log(selectedSubject,'selectedSubject');
    if(selectedCatalogYear == "" || selectedSubject == ""){
        Swal.fire(
            'Required Field',
            'Please select a catalog year and subject to update',
            'warning'
        );
    } else {
        $.ajax({
            url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/update",
            type: "GET",
            data: {
                catalog: selectedCatalogYear,
                subject: selectedSubject
            },
            dataType: 'json',
            headers: {"Authorization": localStorage.getItem('jwt_token')},
        }).done(function(data){
            console.log(data);
            $("#scraperMaintenanceGrid").jsGrid("loadData");
            Swal.fire(
                'Server Processing Complete',
                'Course Data Successfully Updated!',
                'success'
            );
        });
    }
    
}

function updateSchedulesUsingWebScraper(){
    var selectedTerm = $(schedule_term_select).children("option:selected").val();
    var selectedSubject = $(schedule_subject_select).children("option:selected").val();
    //console.log(selectedCatalogYear,'selectedCatalogYear');
    //console.log(selectedSubject,'selectedSubject');
    if(selectedTerm == "" || selectedSubject == ""){
        Swal.fire(
            'Required Field',
            'Please select a term and subject to update',
            'warning'
        );
    } else {
        $.ajax({
            url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/update-schedules",
            type: "GET",
            data: {
                term: selectedTerm,
                subject: selectedSubject
            },
            dataType: 'json',
            headers: {"Authorization": localStorage.getItem('jwt_token')},
        }).done(function(data){
            console.log(data);
            $("#scheduleMaintenanceGrid").jsGrid("loadData");
            Swal.fire(
                'Server Processing Complete',
                'Schedule Data Successfully Updated!',
                'success'
            );
        });
    }
    
}



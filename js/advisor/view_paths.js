$(document).ready(function(){
  loadingDiv = $('#loadingDiv');
  $(loadingDiv).hide();  
  $(document).ajaxStart(function() {
      $(loadingDiv).show();
  })
  .ajaxStop(function() {
      $(loadingDiv).hide();
  });
    $("#criticalPathTable").DataTable({
        "ajax": {
            "url": "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/critical-paths/get-critical-paths",
            // "url": "http://localhost:9000/critical-paths/get-critical-paths",
            "headers": {"Authorization": localStorage.getItem('jwt_token')},
            "type": "GET",
            "cache": true
        },
        // "ajax": function (data, test, settings){
        //     getJobData();
        // },
        pageLength: 5,
        order: [2, "desc"],
        columns: [
          { data: "path_id" },
          { data: "student_uin" },
          { data: "student_name" },
          { data: "student_email" },
          { data: "path_name"},
        ],
        columnDefs: [
          {
            targets: [0],
            visible: false
          },
          {
            targets: 4,
            searchable: true,
            orderable: true,
            render: function(data, type, full, meta) {
              return "<a href='#' data-student-uin='" + full["student_uin"] + "' data-path-name='" + data + "' class='renderStudentPath'>" + data + " </a>";
            }
          }, 
        ],
        dom: "Bfrtip",
        lengthMenu: [
          [ 5, 10, 25, 50, -1 ],
          [ '5 rows', '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        buttons: [
          'pageLength'
        ]
    });
  })
      
// function getJobData(){
//     return $.ajax({
//         url: "http://localhost:9000/critical-paths/get-critical-paths",
//         type: "GET"
//     })
// }

function generateDefaultCriticalPaths(){
  $.ajax({
      // url: "http://localhost:9000/data-entry/run-bls-scraper",
      url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/create-default-critical-paths",
      headers: {"Authorization": localStorage.getItem('jwt_token')},
      method: "GET"
  }).done(function(data){
      Swal.fire(
          'Server Processing Complete',
          'Default Critical Paths Created!',
          'success'
      );
  });    
}


function getStudentCriticalPath(studentUIN, pathName){
  return $.ajax({
    url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/get-critical-path-for-user",
    //url: "http://127.0.0.1:59527/local/critical-paths/get-path-for-major",
    type: "GET",
    data: {
      uin: studentUIN,
      subject: 'CS',
      path: pathName
    },
    dataType: 'json',
    headers: {"Authorization": localStorage.getItem('jwt_token')}
  })
}

function buildPath(nodes, edges) {

  var container = document.getElementById("mynetwork");

  var data = {
    nodes: nodes,
    edges: edges
  };

  var options = {
    nodes: {
      shape: "dot",
      size: 23,
      font: {
        size: 32,
        color: "#ffffff"
      },
      borderWidth: 2
    },
    edges: {
      smooth:
      {
        type: "diagonalCross",
        roundness: 1
      },
      width: 2,
      arrows: "to"
    },
    layout: {
      hierarchical:
      {
        direction: "UD",
        sortMethod: "directed"
      }
    },
    physics: {
      hierarchicalRepulsion: {
        avoidOverlap: 1
      }
    },
    groups: {
      1: {color: {border: '#8B0000',background: '#DC143C'}},
      4: {color: {border: '#006400',background: '#7CFC00'}},
    }
  };

  network = new vis.Network(container, data, options);
}


$("#criticalPathTable").on("click",".renderStudentPath", (e) => {
  let targetPath = $(e.target);
  let pathName = targetPath.data("path-name");
  let studentUIN = targetPath.data("student-uin");

  getStudentCriticalPath(studentUIN, pathName).then(function (data) {
    let nodes = (data["nodes"]);
    let edges = (data["edges"]);

    buildPath(nodes["node"], edges["edge"]);

    $('#criticalPathModal').modal('show');
  })
})



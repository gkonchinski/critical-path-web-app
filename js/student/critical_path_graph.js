var graphNodes = []
var graphEdges = []
$(document).ready(function(){
  loadingDiv = $('#loadingDiv');
  $(loadingDiv).hide();  
  $(document).ajaxStart(function() {
      $(loadingDiv).show();
  })
  .ajaxStop(function() {
      $(loadingDiv).hide();
  });
  //console.log(localStorage);
  var critical_paths;
  $.ajax({
    url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/get-critical-path-names-for-user",
    type: "GET",
    data: {
      uin: localStorage.getItem('uin'),
      subject: 'CS'
    },
    dataType: 'json',
    headers: {"Authorization": localStorage.getItem('jwt_token')},
  }).done(function(paths){
    critical_paths=paths;
    critical_paths.unshift({ "path": "" });
    critical_path_select = $("#critical_path_select");
    $.each(paths,function(index,path){
      if(path.path == ""){
        $(critical_path_select).append('<option value="" selected>Select a critical path to view</option>');
      } else {
        $(critical_path_select).append('<option value="'+path.path+'">'+path.path+'</option>');
      }
    });
    $(critical_path_select).change(function(){
      var selectedPath = $(critical_path_select).children("option:selected").val();
      $.ajax({
        url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/get-critical-path-for-user",
        //url: "http://127.0.0.1:59527/local/critical-paths/get-path-for-major",
        type: "GET",
        data: {
          uin: localStorage.getItem('uin'),
          subject: 'CS',
          path: selectedPath
        },
        dataType: 'json',
        headers: {"Authorization": localStorage.getItem('jwt_token')}
      }).then(function (data) {
        let nodes = (data["nodes"]);
        let edges = (data["edges"]);

        buildPath(nodes["node"], edges["edge"]);
      }); 
    });
  });
  
});



function buildPath(nodes, edges) {

  var container = document.getElementById("mynetwork");
  var data = {
    nodes: nodes,
    edges: edges
  };
  var options = {
    nodes: {
      shape: "dot",
      size: 23,
      font: {
        size: 32,
        color: "#ffffff"
      },
      borderWidth: 2
    },
    edges: {
      smooth:
      {
        type: "diagonalCross",
        roundness: 1
      },
      width: 2,
      arrows: "to"
    },
    layout: {
      hierarchical:
      {
        direction: "UD",
        sortMethod: "directed"
      }
    },
    physics: {
      hierarchicalRepulsion: {
        avoidOverlap: 1
      }
    },
    groups: {
      1: {color: {border: '#8B0000',background: '#DC143C'}},
      4: {color: {border: '#006400',background: '#7CFC00'}},
    }
  };
  network = new vis.Network(container, data, options);
}


// var color = "gray";
// var len = undefined;

// var nodes = []
// var edges = []

// var cs = 0
// var engl = 0
// var math = 0
// var other = 0
// function buildPath(courseData){
//   for(let i = 0; i < courseData.length; i++){
//     if (courseData[i]["subject"] == "CS"){
//       cs++;
//       nodes[i] = ({id: i, label: courseData[i]["subject"] + courseData[i]["course"], group: 0});
//       edges[i] = ({from: 0, to: cs});
//       engl = cs;
//       other = cs;
//     }
//     else if (courseData[i]["subject"] == "ENGL"){
//       engl++;
//       nodes[i] = ({id: i, label: courseData[i]["subject"] + courseData[i]["course"], group: 1});
//       edges[i] = ({from: engl, to: cs});
//       math = engl;
//       other = engl;
//     }
//     else if (courseData[i]["subject"] == "MATH"){
//       math++;
//       nodes[i] = ({id: i, label: courseData[i]["subject"] + courseData[i]["course"], group: 2});
//       edges[i] = ({from: math, to: engl});
//       other = math;
//     }
//     else{
//       other++;
//       nodes[i] = ({id: i, label: courseData[i]["subject"] + courseData[i]["course"], group: 3});
//       edges[i] = ({from: other, to: math});
//     }

//     console.log(nodes[i]);
//     console.log(edges[i]);
//   }
// }
// console.log(testing());
// var nodes = [
//   { id: 0, label: "0", group: 0 },
//   { id: 1, label: "1", group: 0 },
//   { id: 2, label: "2", group: 0 },
//   { id: 3, label: "3", group: 1 },
//   { id: 4, label: "4", group: 1 },
//   { id: 5, label: "5", group: 1 },
//   { id: 6, label: "6", group: 2 },
//   { id: 7, label: "7", group: 2 },
//   { id: 8, label: "8", group: 2 },
//   { id: 9, label: "9", group: 3 },
//   { id: 10, label: "10", group: 3 },
//   { id: 11, label: "11", group: 3 },
//   { id: 12, label: "12", group: 4 },
//   { id: 13, label: "13", group: 4 },
//   { id: 14, label: "14", group: 4 },
//   { id: 15, label: "15", group: 5 },
//   { id: 16, label: "16", group: 5 },
//   { id: 17, label: "17", group: 5 },
//   { id: 18, label: "18", group: 6 },
//   { id: 19, label: "19", group: 6 },
//   { id: 20, label: "20", group: 6 },
//   { id: 21, label: "21", group: 7 },
//   { id: 22, label: "22", group: 7 },
//   { id: 23, label: "23", group: 7 },
//   { id: 24, label: "24", group: 8 },
//   { id: 25, label: "25", group: 8 },
//   { id: 26, label: "26", group: 8 },
//   { id: 27, label: "27", group: 9 },
//   { id: 28, label: "28", group: 9 },
//   { id: 29, label: "29", group: 9 }
// ];
// var edges = [
//   { from: 1, to: 0 },
//   { from: 2, to: 0 },
//   { from: 4, to: 3 },
//   { from: 5, to: 4 },
//   { from: 4, to: 0 },
//   { from: 7, to: 6 },
//   { from: 8, to: 7 },
//   { from: 7, to: 0 },
//   { from: 10, to: 9 },
//   { from: 11, to: 10 },
//   { from: 10, to: 4 },
//   { from: 13, to: 12 },
//   { from: 14, to: 13 },
//   { from: 13, to: 0 },
//   { from: 16, to: 15 },
//   { from: 17, to: 15 },
//   { from: 15, to: 10 },
//   { from: 19, to: 18 },
//   { from: 20, to: 19 },
//   { from: 19, to: 4 },
//   { from: 22, to: 21 },
//   { from: 23, to: 22 },
//   { from: 22, to: 13 },
//   { from: 25, to: 24 },
//   { from: 26, to: 25 },
//   { from: 25, to: 7 },
//   { from: 28, to: 27 },
//   { from: 29, to: 28 },
//   { from: 28, to: 0 }
// ];
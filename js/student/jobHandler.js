$(document).ready(function(){
  $("#jobTable").DataTable({
      "ajax": {
          "url": "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev//jobs/get-job-data",
          "type": "GET",
          "cache": true
      },
      // "ajax": function (data, test, settings){
      //     getJobData();
      // },
      pageLength: 5,
      order: [2, "desc"],
      columns: [
        { data: "id" },
        { data: "job_title" },
        { data: "med_salary" },
        { data: "med_hourly" },
        { data: "entry_level_education" },
        { data: "work_related_experience" },
        { data: "on_the_job_training" },
        { data: "number_jobs" },
        { data: "job_outlook" },
        { data: "employment_change" },
        { data: "last_day_modified"},
        { data: "job_overview" }
      ],
      columnDefs: [
        {
          targets: [0],
          visible: false
        },
        {
          targets: [6],
          visible: false
        }
      ],
      dom: "Bfrtip",
      lengthMenu: [
        [ 5, 10, 25, 50, -1 ],
        [ '5 rows', '10 rows', '25 rows', '50 rows', 'Show all' ]
      ],
      buttons: [
        'pageLength'
      ]
  });
})
    
function getJobData(){
    return $.ajax({
        url: "localhost:9000/jobs/get-job-data",
        type: "GET"
    })
}
//   }
var globalCourseNumbers = [];
var predictedCourses = [];
var termOptions = "";


$(document).ready(function () {
    $("#table_title").hide();
    $("#my-select0").hide();
    $("#my-select1").hide();
    $("#my-select2").hide();
    $("#my-select3").hide();
    $("#my-select4").hide();
    $("#my-select5").hide();
    $("#my-select6").hide();
    $("#my-select7").hide();
    $("#my-select8").hide();
    $("#my-select9").hide();
    $("#my-select10").hide();
    $("#my-select11").hide();
    getTerms().then((terms) => {
        terms["terms"].forEach((item, index) => {
            termOptions += '<option value="' + item["term"] + '">' + item["name"] + '</option>'
        })
    })

    //Form event handler
    $("#updateCoursesTaken").on("click", function (e) {
        e.preventDefault();

        submitCourseTaken();
    })
})

getCourseNumbers().then((courseNumbers) => {
    globalCourseNumbers = courseNumbers;
    activateNumCourseListener();
    activateSemesterListener();
}).catch((error) => {
    alert("getCourseNumbers() failed.");
})


function activateNumCourseListener() {
    $("#numCoursesTaken").on('change', function (e) {
        // console.log("test");
        let val = $(e.target).val();    
        populateCourses(val);
    })
}


function activateSemesterListener() {
    $("#numPlannedSemesters").on('change', function (e) {
        // console.log("test");
        let val = $(e.target).val();
        populateSelector(val);
    })
}

function populateSelector(val){
    $(".courseDropdown").show()
    for(i = 0; i<val; i++){
        
        $('#my-select'+i).multiSelect({
            selectableHeader: "<div class='custom-header'>Semester " +(i+1)+"</div>",
            selectionHeader: "<div class='custom-header'>Planned Courses</div>"
        });
        $('#my-select'+i).multiSelect('addOption', { value: 'test'+i, text: 'test'+i});
        console.log("test");

        $("#my-select"+i).show();
    }
        
}


async function populateCourses(numCourses) {
    $(".dropdown").empty();

    for (i = 0; i < numCourses; i++) {
        $(".dropdown").append(
            '<div class="form-row">' +
            '<div class="form-group col-md-8">' +
            '<input type="text" class="form-control autofill pair_' + i + '" placeholder="Course ' + (i + 1) + '">' +
            '</div>' +
            '<div class="form-group col-md-4">' +
            '<select type="select" class="form-control">' + termOptions + '</select>'  + 
            '</div>' +
            '</div>'
        );
    }

    initAutoComplete();
}


function initAutoComplete() {
    $(".autofill").autocomplete({
        lookup: globalCourseNumbers,
        triggerSelectOnValidInput: false,
        onSelect: function (suggestion) {
            let inputIdentifier = $(this).attr("class").split(/\s+/)[2];

            if ($(".predicted ." + inputIdentifier).length) {
                $(".predicted ." + inputIdentifier).remove();
            }

            getPrereqChain(suggestion["data"]).then((courseData) => {

                if (courseData.length != 0) {
                    predictedTable(courseData, inputIdentifier);
                }
                // else {
                // alert("No courses to suggest")
                // }
            }).catch((error) => {

            })

            renderGraph(suggestion["data"]);
        }
    });
}

function predictedTable(courseData, inputIdentifier) {

    $("#table_title").show();
    // $(".predicted").append('<input type="checkbox" id="matching_' + courseId + '" checked data-toggle="toggle" class="togglebtn">CS150</input><br>');


    for (let i = 0; i < courseData.length; i++) {

        if (predictedCourses.includes(courseData[i]["subject"] + courseData[i]["course"])) {
            //Man Dr.David Bayard just does things sometimes and im just like wow.
            continue;
        }
        else {
            $(".predicted").append(
                '<div class="form-row">' +
                '<div class="form-group col-md-4">' +
                '<input type="checkbox" value="' + courseData[i]["subject"] + courseData[i]["course"] + '"checked data-toggle="toggle" class="togglebtn">' + courseData[i]["subject"] + courseData[i]["course"] + '</input>' +
                '</div>' +
                '<div class="form-group col-md-8">' +
                '<select type="select" class="form-control">' + termOptions + '</select>' +
                '</div>' +
                '</div>'
            )
            predictedCourses.push(courseData[i]["subject"] + courseData[i]["course"]);
        }
    }
    $(".togglebtn").bootstrapToggle({
        on: "Taken",
        off: "Not Taken"
    })
}

function submitCourseTaken() {

    //Get all inputs without select fields
    let inputElements = $(".dropdown input, .predicted input[type='checkbox']:checked");
    let formData = {
        courseData: [],
        uin: localStorage.getItem("uin")
    };

    inputElements.each(function () {

        if ($(this).attr("type") === "text") {
            if ($(this).val() != "") {
                formData["courseData"].push({ "course": $(this).val(), "term": $(this).closest('.form-row').find(":selected").val() });
            }
        }
        if ($(this).attr("type") === "checkbox") {
            formData["courseData"].push({ "course": $(this).val(), "term": $(this).closest('.form-row').find(":selected").val() })
        }
    })

    updateCourseData(formData).then(() => {
        Swal.fire({
            title: 'congratulation',
            text: 'Data Successfully Updated',
            icon: 'success',
        })
    })
}


function updateCourseData(courseData) {
    return $.ajax({
        url: 'https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/update-student-records',
        // url: "http://localhost:9000/courses/update-student-records",
        type: "POST",
        dataType: "text",
        data: {
            courseData: JSON.stringify(courseData)
        }
    })
}


function getTerms() {
    return $.ajax({
        url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/get-terms",
        type: "GET"
    })
}


function getCourseNumbers() {
    return $.ajax({
        // url: 'http://localhost:9000/courses/get-course-suggestions',
        url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/get-course-suggestions",
        type: "GET"
    })
}


function getPrereqChain(course) {

    const courseSubject = course.split(/[0-9]/)[0];
    const courseNumber = course.replace(courseSubject, '');

    return $.ajax({
        // url: 'http://localhost:9000/courses/get-course-prerequisites',
        url: "https://l5zzpnjcy7.execute-api.eu-central-1.amazonaws.com/dev/courses/get-course-prerequisites",
        type: "GET",
        data: {
            'subject': courseSubject,
            'number': courseNumber
        }
    })
}


/*
$('#select-all').click(function () {
    $('#public-methods').multiSelect('select_all');
    return false;
});
$('#deselect-all').click(function () {
    $('#public-methods').multiSelect('deselect_all');
    return false;
});
$('#select-100').click(function () {
    $('#public-methods').multiSelect('select', ['elem_0', 'elem_1' ..., 'elem_99']);
    return false;
});
$('#deselect-100').click(function () {
    $('#public-methods').multiSelect('deselect', ['elem_0', 'elem_1' ..., 'elem_99']);
    return false;
});
$('#refresh').on('click', function () {
    $('#public-methods').multiSelect('refresh');
    return false;
});
$('#add-option').on('click', function () {
    $('#public-methods').multiSelect('addOption', { value: 42, text: 'test 42', index: 0 });
    return false;
});

*/




// var counter = 1;
// var limit = 3;
// function addInput(divName) {
//     if (counter == limit) {
//         alert("You have reached the limit of adding " + counter + " inputs");
//     }
//     else {
//         var newdiv = document.createElement('div');
//         newdiv.innerHTML = "Entry " + (counter + 1) + " <br><input type='text' name='myInputs[]'>";
//         document.getElementById(divName).appendChild(newdiv);
//         counter++;
//     }
// }